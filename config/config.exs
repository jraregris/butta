# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :butta,
  ecto_repos: [Butta.Repo]

# Configures the endpoint
config :butta, ButtaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "s2fg1kxb06qyI1FPl6zvOspMYRqpOQLkRqZ5M2ThKdNzbR0pZZSkaSLaM9/Fhmsa",
  render_errors: [view: ButtaWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Butta.PubSub,
  live_view: [signing_salt: "myvgdira"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
