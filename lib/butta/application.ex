defmodule Butta.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Butta.Repo,
      # Start the Telemetry supervisor
      ButtaWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Butta.PubSub},
      # Start the Endpoint (http/https)
      ButtaWeb.Endpoint
      # Start a worker by calling: Butta.Worker.start_link(arg)
      # {Butta.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Butta.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ButtaWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
