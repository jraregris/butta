defmodule Butta.Repo do
  use Ecto.Repo,
    otp_app: :butta,
    adapter: Ecto.Adapters.Postgres
end
