defmodule ButtaWeb.ItemLive do
  use ButtaWeb, :live_view

  alias Butta.Items

  def mount(_params, _session, socket) do
    Items.subscribe()
    {:ok, fetch(socket)}
  end

  def handle_event("add", %{"item" => item}, socket) do
    Items.create_item(item)

    {:noreply, socket}
  end

  def handle_event("wipe", %{}, socket) do
    Items.wipe_done()

    {:noreply, socket}
  end

  def handle_event("toggle_done", %{"id" => id}, socket) do
    item = Items.get_item!(id)
    Items.update_item(item, %{done: !item.done})
    {:noreply, socket}
  end

  def handle_info({Items, [:item | _], _}, socket) do
    {:noreply, fetch(socket)}
  end

  defp fetch(socket) do
    assign(socket, items: Items.list_items())
  end
end
